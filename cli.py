import argparse
import shutil
import os
import json

parser = argparse.ArgumentParser(description='Move a file to a directory')

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

# command
parser.add_argument('file', help='Pilih lokasi file')
parser.add_argument("-t", action='store_true', help='Flag')
parser.add_argument("-json", action='store_true', help='Ubah ke json')
parser.add_argument("-text", action='store_true', help='Ubah ke text')
parser.add_argument("-o", type=dir_path, help='Pindah file')

args = parser.parse_args()

# extension
dir_name, file_name = os.path.split(args.file)
base_name, extension = os.path.splitext(file_name)
new_json = '.json'
new_txt = '.txt'

# cek command
if args.t:
    if not os.path.isfile(args.file):
        print(f"Error: file '{args.file}' does not exist")
        exit(1)

    i = 1
    result = {}
    with open(args.file) as f:
        lines = f.readlines()
        for line in lines:
            # print(line.split(' '))
            result[i] = {'log': line}
            i += 1

    if args.json:

        # option copy and pindah directory
        if args.o:
            # file and lokasi
            dir_loc = os.path.abspath(args.o)
            new_file_json = os.path.join(dir_loc, base_name + new_json)

            # copy and change file
            move = shutil.copy(args.file, dir_loc)
            os.rename(move, new_file_json)

            # jadiin json
            with open(new_file_json, 'w') as file:
                result = json.dump(result, file)
                print('Berhasil copy dan pindah file menjadi json')

        else:
            with open(args.file, 'w') as file:
                result = json.dump(result, file)
                
            new_file = os.path.join(dir_name, base_name + new_json)
            os.rename(args.file, new_file)
            print('Berhasil ubah file menjadi json')

    
    else:
        if args.o:
            dir_loc = os.path.abspath(args.o)
            new_file_text = os.path.join(dir_loc, base_name + new_txt)

            move = shutil.copy(args.file, dir_loc)
            os.rename(move, new_file_text)
            print('Berhasil copy dan pindah file menjadi text')

        else:
            new_file = os.path.join(dir_name, base_name + new_txt)
            os.rename(args.file, new_file)
            print('Berhasil ubah file menjadi text')

else:
    new_file = os.path.join(dir_name, base_name + new_txt)
    os.rename(args.file, new_file)
    print('Berhasil')

